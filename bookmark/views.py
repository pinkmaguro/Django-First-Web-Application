from django.shortcuts import render, reverse
from django.http import HttpResponse, HttpResponseRedirect
from .models import Bookmark
from .forms import BookmarkForm
# Create your views here.

def index(request):
    print('[INDEX]')
    bookmarks = Bookmark.objects.all().order_by('-created_at')
    context = {
        'bookmarks': bookmarks,
    }
    return render(request, 'bookmark/index.html', context)

def bookmark(request, id):
    print('[BOOKMARK]', id)
    

def new_bookmark(request):
    print('[NEW_BOOKMARK]')
    pass

def edit_bookmark(request, id):
    print('[EDIT_BOOKAMRK]', id)
    bookmark = Bookmark.objects.get(id=id)

    if request.method != 'POST':
        print('[NOT POST]')
        form = BookmarkForm(instance=bookmark)
    else:
        print('[POST]')
        form = BookmarkForm(instance=bookmark, data=request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('bookmark:index'))

    context = {
        'form': form,
        'bookmark': bookmark,
    }

    return render(request, 'bookmark/edit.html', context)