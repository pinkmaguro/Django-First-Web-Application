from django.urls import re_path
from . import views

urlpatterns = [
    re_path(r'^$', views.index, name='index'),
    re_path(r'^(?P<id>\d+)/$', views.bookmark, name='bookmark'),
    re_path(r'^edit/(?P<id>\d+)/$', views.edit_bookmark, name='edit'),
    re_path(r'^new/$', views.new_bookmark, name='new'),
]