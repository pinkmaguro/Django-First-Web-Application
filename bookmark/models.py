from django.db import models

# Create your models here.

class Bookmark(models.Model):
    title = models.CharField(max_length=200)
    url = models.CharField(max_length=300)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title
